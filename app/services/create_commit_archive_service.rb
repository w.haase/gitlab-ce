class CreateCommitArchiveService
  def execute (project, commit)
    path = Gitlab.config.gitlab.repository_downloads_path

    repository = project.repository

    commit_archive_path = repository.archive_commit(commit, path)

    unless commit_archive_path
      false
    else
      commit_archive_path
    end
  end
end
