class CreateComparePatchService
  def execute (project, from, to)
    path = Gitlab.config.gitlab.repository_downloads_path

    repository = project.repository

    compare_patch_path = repository.archive_diff(from, to, path, "changes.zip")

    unless compare_patch_path
      false
    else
      compare_patch_path
    end
  end
end
