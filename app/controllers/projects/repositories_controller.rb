class Projects::RepositoriesController < Projects::ApplicationController
  # Authorize
  before_filter :authorize_read_project!
  before_filter :authorize_code_access!
  before_filter :require_non_empty_project

  def archive
    unless can?(current_user, :download_code, @project)
      render_404 and return
    end

    file_path = ArchiveRepositoryService.new.execute(@project, params[:ref], params[:format])

    if file_path
      # Send file to user
      response.headers["Content-Length"] = File.open(file_path).size.to_s
      send_file file_path
    else
      render_404
    end
  end


  def archive_commit
    unless can?(current_user, :download_code, @project)
      render404 and return
    end

    #Strip Whitespace
    params["commit"] = params["commit"].strip
    commit_archive_path = CreateCommitArchiveService.new.execute @project, params["commit"]

    if commit_archive_path
      response.headers["Content-Length"] = File.open(commit_archive_path).size.to_s
      send_file commit_archive_path
    else
      render404
    end
  end


  def patch
    unless can?(current_user, :download_code, @project)
      render 404 and return
    end

    #Remove leading and trailing whitespace from branchnames or commit hashes
    params["from"] = params["from"].strip
    params["to"] = params["to"].strip

    #TODO: Create Patch
    file_path = CreateComparePatchService.new.execute(@project, params["from"], params["to"])

    if file_path
      # Send file to user
      response.headers["Content-Length"] = File.open(file_path).size.to_s
      send_file file_path
    else
      render_404
    end
  end
end
